new Vue({
   el: 'body',
   
   data:{
      nombre: 'Pedro', 
   },
   
   methods: {
      saludar: function( _evt){
         alert('Buenas Tardes ' + this.nombre);
         console.log(_evt);
      },
      
      enviar: function(){
         alert('Se esta enviando el formulario.')
       },
      
      teclaPulsada: function(_evt){
         console.log(_evt.code);
      },
   },
   
});